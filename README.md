# Travel Tracker #

Example Android app for dropping pins and adding comments on a Google Map.

### Topics addressed: ###

* Working with Google Maps
* Getting user's location
* Persisting data with SQLite database
* Using AsyncTask