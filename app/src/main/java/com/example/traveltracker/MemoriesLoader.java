package com.example.traveltracker;

import android.content.Context;
import android.database.Cursor;

/**
 * Created by jrbrinkley on 7/28/15.
 */
public class MemoriesLoader extends DbCursorLoader {
    private MemoriesDataSource mDataSource;

    public MemoriesLoader(Context context, MemoriesDataSource memoriesDataSource) {
        super(context);
        mDataSource = memoriesDataSource;
    }


    @Override
    protected Cursor loadCursor() {
        return mDataSource.allMemoriesCursor();
    }
}
