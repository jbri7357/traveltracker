package com.example.traveltracker;

import java.io.Serializable;

/**
 * Created by jrbrinkley on 7/27/15.
 */
public class Memory implements Serializable{
    long id;
    double latitude;
    double longitude;
    String city;
    String country;
    String notes;
}
